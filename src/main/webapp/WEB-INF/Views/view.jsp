<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<html>
<head>
<title>Table</title>
</head>

<body>
	<h1>Page No : ${pageNo}</h1>
    <table border='1' cellpadding='4' width='60%'>
    	<thead>
	    	<tr>
	    		<th>Id</th>
	    		<th>Name</th>
	    		<th>Category</th>
	    		<th>Salary</th>
	    	</tr>
    	</thead>
    	<tbody>
    	<c:forEach var="p" items="${products}">
			<tr>
				<td>${p.productId }</td>	
				<td>${p.name }</td>
				<td>${p.category }</td>
				<td>${p.price }</td>
			</tr>
		</c:forEach>
		</tbody>
	</table>
	<c:forEach var="page" begin="1" end="${noOfPages}">
		<a href="products?pageNo=${page}">${page}</a>
	</c:forEach>		
</body>
</html>  
