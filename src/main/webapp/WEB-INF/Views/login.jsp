<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Login Page</title>

</head>

<body>
	<form action="/authenticate"align="center" method="post">
		<input type="text" name="uname" class="form-control" placeholder="Username">
		<input type="password" name="password" class="form-control" placeholder="Password">
		<h5 class="msg">${message }</h5>
		<input type="submit" class="btn btn-primary" value="Login"/>
	</form>
</body>
</html>
