package com.cdac.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.view.RedirectView;

import com.cdac.pojo.Product;
import com.cdac.service.ProductService;

@Controller
public class PageController {

	@Autowired
	ProductService productService;
	
	public PageController() { } 
	
	
	@GetMapping("/login")
	public String login()
	{
		return "login";
	}  

	@PostMapping("/authenticate")
	public RedirectView authenticate(@RequestParam("uname") String uname,@RequestParam("password") String password,ModelMap model)
	{  
		// Database Authentication
		if(uname.equals("user") && password.equals("user"))
			return new RedirectView("/products?pageNo=1");
		return new RedirectView("login"); 
	}

	
	@GetMapping("/products")
	public String getProductsList(ModelMap model, @RequestParam int pageNo) {
		
		// Just Change this as per need
		int numberOfItemsPerPage = 5;
		
		// To get data from database
		List<Product> allProducts = productService.getAllProducts();
		
		// To calculate number of pages to show depending on total number of table item
		int numberOfPages = (allProducts.size()%numberOfItemsPerPage)==0?  allProducts.size()/numberOfItemsPerPage : (allProducts.size()/numberOfItemsPerPage + 1);
		
		
		int firstIndex = (pageNo -1) * numberOfItemsPerPage;
		int lastIndex = firstIndex + (numberOfItemsPerPage);
		
		if(lastIndex > allProducts.size())
			lastIndex = allProducts.size();
		
		allProducts = allProducts.subList(firstIndex, lastIndex);
		
		// Adding all required variables into the model
		model.put("pageNo", pageNo);
		model.put("noOfPages", numberOfPages);
		model.put("products", allProducts);
		
		return "view";
	}

}
