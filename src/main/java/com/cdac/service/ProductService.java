package com.cdac.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cdac.dao.ProductDao;
import com.cdac.pojo.Product;

@Service
public class ProductService {
	
	@Autowired
	ProductDao productDao;
	
	public ProductService() { }
	
	public Product getProductById(int productId) {
		Optional<Product> optionalProdcut = productDao.findById(productId);
		if(optionalProdcut.isPresent()) {
			return optionalProdcut.get();
		}
		return null;
	}
	
	public List<Product> getAllProducts(){
		return productDao.findAll();
	}
	
	public Boolean addProduct(Product product) {
		if(productDao.save(product)!=null)
			return true;
		return false;
	}

	public Boolean deleteProduct(int id) {
		Optional<Product> optionalProdcut = productDao.findById(id);
		if(optionalProdcut.isPresent()) {
			Product product = optionalProdcut.get();
			productDao.delete(product);
			return true;
		}
		return false;
	}
}
