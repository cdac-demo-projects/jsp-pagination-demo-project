package com.cdac.pojo;

import javax.persistence.*;

@Entity
@Table(name = "products")
public class Product {
	
	private int productId;
	private String name;
	private double price;
	private String category;
	
	public Product() { }

	public Product(int productId, String name, double price, String category) {
		super();
		this.productId = productId;
		this.name = name;
		this.price = price;
		this.category = category;
	}

	@Column
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	public int getProductId() {
		return productId;
	}

	public void setProductId(int productId) {
		this.productId = productId;
	}

	@Column
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Column
	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	@Column
	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	@Override
	public String toString() {
		return "Product [productId=" + productId + ", name=" + name + ", price=" + price + ", category=" + category
				+ "]";
	}
	
}
