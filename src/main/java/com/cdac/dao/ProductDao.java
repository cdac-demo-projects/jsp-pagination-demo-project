package com.cdac.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.cdac.pojo.Product;

public interface ProductDao extends JpaRepository<Product, Integer> {

}
